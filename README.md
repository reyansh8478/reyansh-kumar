# README #
KreditBee is an Instant Personal Loan platform for young professionals, where they can avail a Salary Advance of upto Rs. 2 Lac. The platform requires minimal documentation, and the entire process, starting from registration to cash disbursement, generally takes no more than 15 minutes. The application process is completely online, and upon approval, the cash is immediately transferred to the bank account of the user. 

Even if you haven’t taken a loan before, or don’t have a credit card, you can still avail a Personal Loan with KreditBee. We offer loans in varying ticket sizes and repayment tenures, to suit all your foreseen and unforeseen financial dynamics.   


 
Address for Correspondence:   
KreditBee, 3rd floor, The Royal Stone Park, No. 100, Above Honda Showroom, Sakshi Nagar, Pai Layout, Bennigana Halli, Karnataka 560016  

Website : https://www.kreditbee.in/ 
